import model.Guitar;
import model.TypeGuitar;
import service.GuitarService;

public class Main {
    public static void main(String[] args) {

    Guitar guitar1=new Guitar(1,"B.C Rich", TypeGuitar.V_STYLE,"Stock",6);
    Guitar guitar2=new Guitar(2,"Jackson",TypeGuitar.V_STYLE,"EMG",6);
    Guitar guitar3=new Guitar(3,"Gibson",TypeGuitar.LES_PAUL,"Stock",6);
   GuitarService guitarService= new GuitarService();
   guitarService.addGuitars(guitar1);
   guitarService.addGuitars(guitar2);
   guitarService.addGuitars(guitar3);
   guitarService.findTypeGuitar("V_STYLE");
   guitarService.listAllGuitars();






    }

    }
//a.krause