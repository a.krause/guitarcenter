package dao;

import model.Guitar;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;



public class GuitarDao {
    public  void createGuitars(Guitar guitar){

        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
      try {
          session.persist(guitar);
          transaction.commit();
      }catch (Exception e){
          transaction.rollback();
      }finally {
          session.close();
      }

    }
    public  Guitar getGuitarById(int id){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Guitar guitar=null;
        try (Session session=sessionFactory.openSession()){
            guitar=session.find(Guitar.class,id);

        }catch (Exception e){
            e.printStackTrace();
        }return guitar;
    }
    public List<Guitar> allGuitars(){
        List<Guitar> showAllGuitars=new ArrayList<>();
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
        Session session=sessionFactory.openSession();
        String query ="SELECT c FROM Guitar c ";
            try {

                 showAllGuitars=session.createQuery(query,Guitar.class).getResultList();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                session.close();
            }return showAllGuitars;
        }

    public List findGuitarByTYpe(String guitarType){
        SessionFactory sessionFactory= HibernateUtil.getSessionFactory();
            List type=null;

            String query="SELECT c FROM Type c WHERE c.type=:guitarType";
        try (Session session=sessionFactory.openSession();){

            type=  session.createQuery(query).setParameter("type",guitarType)
                    .getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return type;

    }
}
//a.krause
