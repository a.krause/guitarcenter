package model;

import javax.persistence.*;

@Entity
@Table(name = "Guitars")
@NamedQueries({
        @NamedQuery(name = "Guitar.findGuitarByTYpe", query = "select b from Guitar b c.type=:guitarType"),

})

public class Guitar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "Guitar Brand")
    private String brand;

    public Guitar(int id, String brand, TypeGuitar typeGuitar, String picksup, int strings) {
        this.id = id;
        this.brand = brand;
        this.typeGuitar = typeGuitar;
        this.picksup = picksup;
        this.strings = strings;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "Type Guitar" )
    private TypeGuitar typeGuitar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public TypeGuitar getTypeGuitar() {
        return typeGuitar;
    }

    public void setTypeGuitar(TypeGuitar typeGuitar) {
        this.typeGuitar = typeGuitar;
    }

    public String getPicksup() {
        return picksup;
    }

    public void setPicksup(String picksup) {
        this.picksup = picksup;
    }

    public int getStrings() {
        return strings;
    }

    public void setStrings(int strings) {
        this.strings = strings;
    }

    @Column(name = "Pickups")
    private  String picksup;
    @Column(name = "Strings")
    private  int strings;

    @Override
    public String toString() {
        return "Guitar{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", typeGuitar=" + typeGuitar +
                ", picksup='" + picksup + '\'' +
                ", strings=" + strings +
                '}';
    }
}
//a.krause